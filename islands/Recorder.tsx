import { signal } from "@preact/signals";
import { useRef } from "preact/hooks";

const recording = signal<boolean | number>(false);
const histogram = signal<number[]>([]);
// only one recorder can be active at a time
let button: HTMLButtonElement;
let stream: MediaStream;
let interval;
let mediaRecorder: MediaRecorder;

// type for props button element attributes
type RecorderProps = JSX.IntrinsicElements["button"];

export default function Recorder(props: RecorderProps) {
  const ref = useRef<HTMLButtonElement>();
  if (ref.current) {
    button = ref.current;
  }
  return (
    <button
      ref={ref}
      onClick={recording.value ? stopRecording : record}
      title={recording.value ? "Called it" : "Call it"}
      data-recording={recording.value}
      class={`relative focus:outline-none group ring-inset ring ring-[#eee] -mt-8 self-end flex justify-center rounded-full h-16 w-16 text-xl z-10 shadow transition bg-[#000] hover:bg-[cyan] data-[recording=true]:bg-[cyan]`}
      {...props}
    >
      <span
        class={`absolute top-0 left-0 bottom-0 right-0 rounded-full z-10 ${
          recording.value ? "animate-ping bg-[cyan] " : ""
        }`}
      >
      </span>
      {recording.value
        ? (
          <figure
            rel="amplitude-histogram"
            class="relative h-8 flex w-0 gap-2 right-full flex-row-reverse absolute overflow-visible border-none z-[-1]"
          >
            {histogram.value.map((amp, index) => (
              <div
                class="h-full border text-black w-1 rounded bg-white"
                key={amp + index}
                style={{ transform: `scaleY(${Math.max(amp, 0.1)})` }}
              />
            ))}
          </figure>
        )
        : null}
      <svg
        class="h-full p-4 text-white z-20"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          class={recording.value ? "group-hover:hidden" : ""}
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M9 4C9 2.34315 10.3431 1 12 1C13.6569 1 15 2.34315 15 4V12C15 13.6569 13.6569 15 12 15C10.3431 15 9 13.6569 9 12V4ZM13 4V12C13 12.5523 12.5523 13 12 13C11.4477 13 11 12.5523 11 12V4C11 3.44772 11.4477 3 12 3C12.5523 3 13 3.44772 13 4Z"
          fill="currentColor"
        />
        <path
          class={recording.value ? "group-hover:hidden" : ""}
          d="M18 12C18 14.973 15.8377 17.441 13 17.917V21H17V23H7V21H11V17.917C8.16229 17.441 6 14.973 6 12V9H8V12C8 14.2091 9.79086 16 12 16C14.2091 16 16 14.2091 16 12V9H18V12Z"
          fill="currentColor"
        />
        <path
          class={recording.value ? "group-hover:block hidden" : "hidden"}
          d="M7 7H17V17H7V7Z"
          fill="currentColor"
        />
      </svg>
    </button>
  );
}

function stopRecording() {
  recording.value = false;
  const tracks = stream.getTracks();
  tracks.forEach((track) => track.stop());
  clearInterval(interval);
  mediaRecorder.stop();
}

// output per second histogram of audio amplitude
function ampHistogram(mediaRecorder: MediaRecorder): number[] {
  const audio = mediaRecorder.requestData();
  const audioContext = new AudioContext();
  const audioBuffer = audioContext.createBufferSource();
  const analyser = audioContext.createAnalyser();
  const gainNode = audioContext.createGain();

  return histogram;
}

async function record() {
  if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
    stream = await navigator.mediaDevices.getUserMedia(
      {
        audio: true,
      },
    );
    recording.value = Date.now();
    mediaRecorder = new MediaRecorder(stream, { mimeType: "audio/webm" });
    mediaRecorder.start();
    histogramAudio(mediaRecorder);
  } else {
    console.error("getUserMedia not supported on your browser!");
  }
}
