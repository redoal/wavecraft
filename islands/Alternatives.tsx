import { useEffect, useState } from "preact/hooks";
import { Alternative, Feature, Requirement } from "../questions.ts";
import Canvas from "./Canvas.tsx";

export default function Alternatives(
  { data }: { data: Alternative[] },
): JSX.Element {
  const [alternatives, setAlternatives] = useState(data);
  return data.map(
    (alternative: Alternative) => {
      return (
        <form
          method="post"
          action={alternative.action}
          // onchange={`this.querySelector('button[type=submit]').disabled = ${alternative.disabled} || !this.checkValidity()`}
          class="box-border p-16 flex snap-start flex-col"
          data-theme={alternative.domain || "public"}
        >
          <h2 class="text-6xl mb-16">
            {alternative.action
              ? alternative.name.split(" ").map(
                highlighter(
                  alternative.action?.toUpperCase().split("/"),
                ),
              )
              : alternative.name}
          </h2>
          <p class="mb-8 text-3xl font-bold">{alternative.description}</p>
          <ul class="flex flex-col gap-8 mb-8 flex-auto justify-around">
            {alternative.features?.map(feature)}
          </ul> 
          <button
            type="submit"
            disabled={alternative.disabled}
            hidden={!alternative.action}
            class="border disabled:cursor-not-allowed disabled:bg-[grey] disabled:shadow-[cyan] disabled:shadow-inner shadow pointer-events-auto leading-5 self-end self-end rounded bg-[#00f2f2] p-4 outline-none border text-black mb-8"
          >
            {alternative.consequence?.join(" ")}
          </button>
          <p class="text-right mb-8">
            {alternative.encouragement || ""}
          </p>
          <aside class={alternative.support ? "block" : "hidden"}>
            <h4 class="mb-4">We're trusted by</h4>
            <ul class="flex gap-4">
              {alternative.support?.map((user) => {
                return (
                  <a href={user.domain} class="flex border-[black] border p-2 rounded">
                  {user.logo ? (
                      <img href={user.logo} class="w h-full" />
                    ) : null}
                    {user.name}
                  </a>
                );
              })}
            </ul>
          </aside>
        </form>
      );
    },
  );
}

function highlighter(matches: string[]): Function {
  return (text: string) => {
    if (matches.includes(text.toLocaleUpperCase())) {
      return (
        <>
          <span class="transition duration-300 ease-in-out text-black bg-[#ccc] hover:bg-[cyan] rounded p-2 -m-2">
            {text}
          </span>
          {" "}
        </>
      );
    }
    return (
      <>
        <span>{text}</span>
        {" "}
      </>
    );
  };
}

export function requirement(props: Requirement) {
  const {
    type,
    name,
    placeholder,
    options,
    optional,
    hint,
    min,
    value,
    multiple,
  } = props;
  const inputClasses =
    "border rounded font-sans text-2xl my-2 px-2 py-1 max-w-prose checked:bg-[cyan] outline-none focus:border-black appearance-none bg-white invalid:border-dashed invalid:border-[#00f2f2]";
  switch (type) {
    case "radio":
      return options?.map((option, index) => (
        <>
          <input
            class={inputClasses}
            id={name + option.value}
            name={name}
            placeholder={placeholder}
            type={type}
            required={!optional}
            checked={option.value === name || index === 0}
          />
          <label for={name + option.value}>
            <h3 class="text-xl">{option.value}</h3>
            <p>{option.label}</p>
          </label>
        </>
      ));
    case "select":
      return (
        <select
          id={name}
          class={inputClasses + " checked:bg-[yellow]"}
          name={name}
          placeholder={placeholder}
        >
          {options?.map((option) => (
            <option value={option.value}>
              {option.label}
            </option>
          ))}
        </select>
      );
    case "curve":
      return (
        <Canvas
          name={name}
          title={hint}
          class="cursor-move w-64 h-64 bg-gradient-to-t from-[#00fefe33] to-[#00fefe11] border text-white bg-neutral-200 rounded"
        />
      );

    case "date":
      return (
        <input
          class={inputClasses}
          name={name}
          placeholder={placeholder}
          value={value}
          type={type}
          min={min}
          required={!optional}
        />
      );
    case "checkbox":
      return (
        <div class="flex gap-3">
          <label for={name}>
            {name}
          </label>
          <input
            name={name}
            type="checkbox"
            class="checked:bg-[cyan] rounded w-4 h-4 bg-black "
          />
        </div>
      );
    default:
      return (
        <input
          class={inputClasses}
          name={name}
          title={hint}
          multiple={multiple}
          placeholder={placeholder}
          value={value}
          type={type}
          min={min}
          required={!optional}
        />
      );
  }
}

export function feature(props: Feature) {
  const { name, requirements, description, resource } = props;
  const [resources, setResources] = useState([]);
  useEffect(() => {
    if (resource?.path) {
      fetch(resource.path)
        .then((response) => response.json())
        .then((data) => {
          setResources(data);
        });
    }
  }, []);
  return (
    <li class="flex flex-col pl-0">
      <h3 class="text-2xl">{name}</h3>
      <div hidden={!!requirements?.length}>
        <p>{description}</p>
      </div>
      {requirements?.length
        ? (
          <fieldset>
            <legend>{description}</legend>
            <div class="flex flex-col gap-4 portrait:flex-col flex-wrap mt-2">
              {requirements?.map(requirement)}
            </div>
            <details
              id={name + "-resource"}
              open={false}
              hidden={!resource}
            >
              <summary>
                Resources <span class="text-sm">({resource?.length})</span>
              </summary>
              <ul id={name + "-list"}>
              </ul>
            </details>
          </fieldset>
        )
        : null}
    </li>
  );
}
