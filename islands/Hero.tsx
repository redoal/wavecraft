import { useEffect } from "preact/hooks";
import { signal, Signal } from '@preact/signals'
import { createRef } from "preact";
import { Logo } from "../components/Logo.tsx";
import { Button } from "../components/Button.tsx";

const waitlist = signal(false)

export function Hero(props) {
  const logo = createRef();
  const header = createRef();
  useEffect(() => {
    if (!!localStorage.getItem('waitlist')) waitlist.value = true;
    document.addEventListener("scroll", onScroll);
    function onScroll(event) {
      logo.current.setAttribute(
        "style",
        `transform: translateY(${-scrollY * 1.5}px)`,
      );
      header.current.lastElementChild.setAttribute(
        "style",
        `transform: translateY(${-scrollY * 0.75}px)`,
      );
    }
    return () => {
      document.removeEventListener("scroll", onScroll);
    };
  }, []);
  return (
    <header
      ref={header}
      class="relative snap-start h-screen box-border p-16 flex flex-col justify-between items-start z-10"
    >
      <nav class="flex flex-col w-full gap-4 items-center">
        <ul class="flex flex-row-reverse self-end">
          { waitlist.value ? (
              <div class="border p-2 rounded">Thanks for signing up. We'll keep you posted</div>
            ) : (
              <li>
                <a
                  class="bg-[#00f2f2] text-black px-3 py-2 inline rounded"
                  href="#waitlist"
                >
                  Join the waitlist
                </a>
                <form
                  id="waitlist"
                  class="target:flex text-sm flex-col landscape:flex-row absolute top-0 right-0 -translate-y-4 bg-white items-center m-8 p-8 gap-4 w-md shadow hidden z-10"
                  action="/api/waitlist"
                  method="post"
                >
                  <h3>Get to know when we're up and running</h3>
                  <input name="name" placeholder="Name" class="w-full p-2 border rounded" />
                  <input
                    class="rounded border p-2 w-full"
                    name="email"
                    type="email"
                    placeholder="Email"
                  />
                  <Button onClick={_ =>localStorage.setItem('waitlist', 'true')}>I want this</Button>
                </form>
              </li>
            )
          }
        </ul>
        <div ref={logo} class="flex gap-4 items-center w-full justify-start">
          <Logo class="text-[#00f2f2] w-32 h-32" />
          <h1 class="text-7xl font-bold">
            <span class="-scale-x-100 inline-block">R</span>edoal
          </h1>
        </div>
      </nav>
      {props.children}
    </header>
  );
}
