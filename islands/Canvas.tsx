import { Signal, signal } from "@preact/signals";
import { useEffect, useRef } from "preact/hooks";
import type { Ref } from "preact";

interface CanvasProps {
  points?: Signal<number[][]>;
  name: string;
  ref?: Ref<HTMLCanvasElement>;
}

const points = signal([]);
const suggestions = signal([]);

export default function Canvas(props: CanvasProps) {
  const {
    ref = useRef<HTMLCanvasElement>(),
    name,
    ...attrs
  } = props;

  useEffect(() => {
    const canvas = ref.current;
    if (!canvas) return;
    let { width, height } = canvas.getBoundingClientRect();
    const ctx = ref.current.getContext("2d");
    canvas.width = width;
    canvas.height = height;
    let line = false;

    canvas.addEventListener("touchstart", touchStart);
    canvas.addEventListener("touchmove", touchMove);
    canvas.addEventListener("touchend", touchEnd);
    canvas.addEventListener("mousedown", mouseDown);
    canvas.addEventListener("mousemove", mouseMove);
    canvas.addEventListener("mouseup", mouseUp);
    canvas.addEventListener("drawn", onDraw);
    addEventListener("resize", onResize);

    function onResize() {
      const rect = canvas.getBoundingClientRect();
      canvas.width = width = rect.width;
      canvas.height = height = rect.height;
    }

    function touchStart(e) {
      e.preventDefault();
      line = true;
      ctx.clearRect(0, 0, width, height);
      ctx.strokeStyle = "white";
      ctx.beginPath();
      points.value = [];
    }

    function touchMove(e) {
      e.preventDefault()
      if (line) {
        if (points.value.length === 0) {
          points.value = [[e.touches[0].offsetX, e.touches[0].offsetY]]
        } else {
          const [x, y] = points.value[points.value.length - 1]
          ctx.moveTo(x, y)
          ctx.lineTo(e.touches[0].offsetX, e.touches[0].offsetY)
          ctx.stroke()
          points.value = [...points.value, [e.touches[0].offsetX, e.touches[0].offsetY]]
        }
      }
    }

    function touchEnd(e) {
      e.preventDefault();
      canvas.dispatchEvent(new CustomEvent("drawn", { detail: points.value }));
      line = false;
    }

    function mouseDown(e) {
      e.preventDefault();
      ctx.clearRect(0, 0, width, height);
      line = true;
      ctx.beginPath();
      ctx.moveTo(width / 2, height / 2);
      ctx.strokeStyle = "white";
      points.value = [];
    }

    function mouseMove(e) {
      e.preventDefault();

      if (line) {
        // draw line from last point to to mouse position
        ctx.beginPath();
        if (points.value.length === 0) {
          points.value = [[e.offsetX, e.offsetY]];
        } else {
          const [x, y] = points.value[points.value.length - 1];
          ctx.moveTo(x, y);
          ctx.lineTo(e.offsetX, e.offsetY);
          ctx.stroke();
          points.value = [...points.value, [e.offsetX, e.offsetY]];
        }
      }
    }

    function mouseUp(e) {
      e.preventDefault();
      if (line && points.value.length > 1) {
        canvas.dispatchEvent(
          new CustomEvent("drawn", {
            detail: points.value,
          }),
        );
        line = false;
        // history.push(inputPoints);
      }
    }

    function onDraw(event) {
      const hashTable = {};
      console.log(event.detail);
      suggestions.value = [
        event.detail,
        ...suggestions.value,
      ];
    }

    return () => {
      canvas.removeEventListener("touchstart", touchStart);
      canvas.removeEventListener("touchmove", touchMove);
      canvas.removeEventListener("touchend", touchEnd);
      canvas.removeEventListener("mousedown", mouseDown);
      canvas.removeEventListener("mousemove", mouseMove);
      canvas.removeEventListener("mouseup", mouseUp);
      canvas.removeEventListener("drawn", onDraw);
      removeEventListener("resize", onResize);
    };
  }, []);

  return (
    <div class="flex justify-stretch">
      <input name={name} hidden value={points.value.toString()} />
      <div class="z-10">
        <canvas ref={ref} {...attrs} />
      </div>
      <div class="flex gap-4 p-4 flex-auto relative z-0 justify-stretch items-start">
        {suggestions.value.map((suggestion) => (
          <a
            class="hover:border rounded aspect-square w-min h-full "
            href={"/" + suggestion.toString()}
          >
            <svg
              onClick={() => points.value = suggestion}
              class="object-cover h-full aspect-square"
              viewBox="0 0 400 400"
            >
              <path d={toPathString(suggestion)} stroke="white" fill="none" />
            </svg>
          </a>
        ))}
      </div>
    </div>
  );
}

// x y to svg path string
function toPathString(points: number[][]) {
  const [startX, startY] = points[0];
  let pathString = `M${startX} ${startY}`;
  for (let i = 1; i < points.length; i++) {
    pathString += `L${points[i][0]} ${points[i][1]}`;
  }
  return pathString;
}
