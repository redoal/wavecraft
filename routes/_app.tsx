import { AppProps } from "$fresh/server.ts";

export default function App({ Component }: AppProps) {
  // console.log(props)
  return (
    <html class="snap-y snap-proximity">
      <head>
        <meta charset="utf-8" />
        <title>Яedoal</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="CurveSpace" />
        <meta name="theme-color" content="#000000" />
        <link rel="icon" href="/icon.svg" />
        <link rel="shortcut icon" href="/icon.svg" />
        <link rel="stylesheet" href="/app.css" />
        <script src="/worklet/paint/index.js"></script>
      </head>
      <body class="relative flex flex-col items-stretch bg-neutral-100 text-neutral-900 max-w-screen overflow-x-hidden">
        <Component />
      </body>
    </html>
  );
}
