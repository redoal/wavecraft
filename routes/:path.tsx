import { Logo } from "../components/Logo.tsx";
import { PageProps } from "$fresh/server.ts";
import Recorder from "../islands/Recorder.tsx";

const DENO_KV_PROD_UUID = Deno.env.get("DENO_KV_PROD_UUID");

export default async function pathRoute(props: PageProps) {
  // const kv = await Deno.openKv(DENO_KV_PROD_UUID ? `https://api.deno.com/databases/${DENO_KV_PROD_UUID}/connect` : undefined);
  // TODO: fetch paths based on hash key
  const url = new URL(props.url);
  // const pathData = await kv.get(["path", url.pathname.slice(1)]);
  // if (!pathData) console.log("no pathdata for key:", url.pathname);

  return (
    <div class="flex h-screen">
      <nav class="border-r border-[#eee] w-32 lg:w-64 md:hover:md:w-64 flex flex-col transition-[width] duration-300 gap-2 items-start">
        <a href="/" class="m-2">
          <Logo class="text-black transition hover:text-[cyan] max-h-24 w-full aspect-square h-full" />
        </a>
        <div class="flex w-full bg-[#eee]">
          <svg
            viewBox="0 0 400 400"
            class="max-h-24 self-start h-min w-full"
          >
            <path
              d={pointsToPath(
                url.pathname.slice(1).split(",").map((n) => Number(n)),
              )}
              fill="none"
              stroke-width="8"
              stroke="black"
            />
          </svg>
        </div>
      </nav>
      <div class="overflow-hidden flex flex-col min-h-full px-16 flex-auto">
        <header class="flex justify-between items-center py-4">
          <h3>No story here yet…</h3>
          <svg
            class="w-8 h-8 p-1 mx-4"
            onclick="navigator.canShare ? navigator.share({ title: 'Discussion on a path', url: location.host }) : navigator.clipboard.writeText(location)"
            viewBox="0 0 24 24"
            title="Bring people onboard"
            fill="currentColor"
            stroke="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M16.9498 5.96781L15.5356 7.38203L13 4.84646V17.0421H11V4.84653L8.46451 7.38203L7.05029 5.96781L12 1.01807L16.9498 5.96781Z" />
            <path d="M5 20.9819V10.9819H9V8.98193H3V22.9819H21V8.98193H15V10.9819H19V20.9819H5Z" />
          </svg>
        </header>
        <div class="relative h-64 flex bg-[#ccc] p-2 gap-2 items-start -mx-16 bold text-right">
          <div
            class="w-full h-full absolute top-0 left-0"
            style={{
              background: "paint(starry)",
              "--path": url.pathname.slice(1),
            }}
          />
        </div>
        <Recorder />
        <div class="h-full w-full -my-8">
          <div class="object-cover aspect-square h-full -mx-16 box-sizing" style={{background: 'paint(paths)'}}/>
        </div>
      </div>
    </div>
  );
}

function pointsToPath(points: number[]) {
  let path = "M" + points[0] + "," + points[1];
  for (let i = 2; i < points.length; i += 2) {
    path += " L" + points[i] + "," + points[i + 1];
  }
  return path;
}
