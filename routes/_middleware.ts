import { MiddlewareHandlerContext } from "$fresh/server.ts";
import { getCookies, setCookie } from "https://deno.land/std/http/cookie.ts";

interface State {
  kv: Deno.Kv;
}

export async function handler(
  req: Request,
  ctx: MiddlewareHandlerContext<State>,
) {
  const cookie = getCookies(req.headers);
  ctx.state.kv = await Deno.openKv(); // Likely unperformant hehe
  if (req.method === "POST") {
    const answer = await req.formData();
    const { pathname } = new URL(req.url);
    const timestamp = Date.now();
    const data = Object.fromEntries(answer);
    await ctx.state.kv.set([pathname, timestamp], data)
    console.log('stored', data)
    const headers = new Headers()
    setCookie(headers, {
      name: pathname,
      value: timestamp,
      domain: Deno.env.get('DOMAIN'),
      path: '/'
    })
    headers.append('Location', '/')
    return new Response("", {
      status: 301,
      headers
    });
  }
  // For api middleware
  const authorization = req.headers.get("Authorization");
  if (!authorization) {
    const response = await ctx.next();
    setCookie(response.headers, {
      name: Deno.env.get("COOKIE_UI_GUIDE_KEY") || "",
      value: Deno.env.get("UI_GUIDE_VALUES.SIGNED_OUT") || "",
      domain: Deno.env.get("DOMAIN"),
      path: "/",
      expires: new Date().setDate(new Date().getDate() + 14),
      secure: !!Deno.env.get("SECURE"),
    });
    return response;
  }
  const [bearer, token] = authorization.split(" ");
  if (bearer !== "Bearer") {
    return ctx.next();
  }
  const user = await ctx.state.db.collection("users").findOne({ token });
  if (!user) {
    ctx.state.user = null;
    return ctx.next();
  } else {
    ctx.state.user = user;
  }
  return ctx.next();
}
