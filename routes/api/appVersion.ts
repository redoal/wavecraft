import { HandlerContext } from "$fresh/server.ts";

export const handler = (_req: Request, _ctx: HandlerContext): Response => {
  return new Response({ appVersion: Deno.env.get("APP_VERSION") });
};
