import { S3, S3Bucket } from "s3";
import { MiddlewareHandler, MiddlewareHandlerContext } from "$fresh/server.ts";

export const handler: MiddlewareHandler = async (
  _req: Request,
  ctx: MiddlewareHandlerContext,
) => {
  ctx.state.s3 = new S3({
    accessKeyID: Deno.env.get("AWS_ACCESS_KEY_ID")!,
    secretKey: Deno.env.get("AWS_SECRET_ACCESS_KEY")!,
    region: "eu-west-1",
    endpointURL: Deno.env.get("S3_ENDPOINT_URL"),
  });
  const response: Response = await ctx.next();
  return ctx.next();
};

// Create a new bucket.
// let bucket = await s3.createBucket("test", { acl: "private" });

// Get an existing bucket.
// bucket = s3.getBucket("test");

// Create a bucket instance manuely.
// bucket = new S3Bucket({
//   accessKeyID: Deno.env.get("AWS_ACCESS_KEY_ID")!,
//   secretKey: Deno.env.get("AWS_SECRET_ACCESS_KEY")!,
//   bucket: "test",
//   region: "us-east-1",
//   endpointURL: Deno.env.get("S3_ENDPOINT_URL"),
// });

// const encoder = new TextEncoder();

// Put an object into a bucket.
// await bucket.putObject("test", encoder.encode("Test1"), {
//   contentType: "text/plain",
// });

// Retrieve an object from a bucket.
// const { body } = await bucket.getObject("test");
// const data = await new Response(body).text();
// console.log("File 'test' contains:", data);

// List objects in the bucket.
// const list = bucket.listAllObjects({});
// for await (const obj of list) {
//   console.log("Item in bucket:", obj.key);
// }

// Delete an object from a bucket.
// await bucket.deleteObject("test");
