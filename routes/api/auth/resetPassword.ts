import {
  deleteCookie,
  getCookies,
  setCookie,
} from "https://deno.land/std/http/cookie.ts";
import { HandlerContext } from "$fresh/server.ts";

export const handler = async (_req: Request, ctx: HandlerContext): Response => {
  try {
    const { token, emailAddress, password } = await _req.json();
    const apiRes = await fetch(
      new URL("/v1/password_reset", ctx.state.sstURL as string),
      {
        method: "POST",
        body: JSON.stringify({
          verificationKey: token,
          emailAddress,
          password,
        }),
      },
    ).then((response) => response.json());
    const response = new Response(null, { status: apiRes.status });
    setCookie(response.headers, {
      name: Deno.env.get("COOKIE_UI_GUIDE_KEY") || "",
      value: Deno.env.get("UI_GUIDE_VALUES.SIGNED_OUT") || "",
      domain: Deno.env.get("DOMAIN"),
      path: "/",
      expires: new Date().setDate(new Date().getDate() + 14),
      secure: !!Deno.env.get("SECURE"),
    });
    return response;
  } catch (error) {
    console.error(error);
    return new Response(error, { status: 400 });
  }
};
