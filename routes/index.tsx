import { Head, IS_BROWSER } from "$fresh/runtime.ts";
import { PageProps } from "$fresh/server.ts";
import { Hero } from "../islands/Hero.tsx";
import Alternatives from "../islands/Alternatives.tsx";
import { HandlerContext, Handlers } from "$fresh/server.ts";
import { questions } from "../questions.ts";

export const handler: Handlers = {
  async GET(req: Request, ctx: HandlerContext) {
    console.log(req.headers.get("cookie"));
    let resp = await ctx.render({
      data: questions[0] || [],
      history: [],
    });
    resp.headers.set("X-redoal-UUID", crypto.randomUUID());
    return resp;
  },
};

/**
 * Anyones first impression where we cast the net as wide as possible to give our
 * visitor agency to interact. We want to give them a sense of what we do and how
 * we do it. With an invitation to explore further.
 */
export default function Home(props: PageProps): JSX.Element {
  const { data, history } = props.data;
  const url = new URL(props.url);
  const [campaign] = history.filter((act) => !!act.Strategy);
  const [user] = history.filter((act) => !!act.Name);
  // A list of all alternatives in a form
  return (
    <>
      <canvas
        id="orb-canvas"
        class="sticky h-screen bottom-0 w-full z-0 -mb-[100vh] mix-blend-multiply"
      />
      <Hero>
        <h1 class="text-xl lg:text-4xl font-bold order-3">{data.name}</h1>
        <p class="text-lg order-2 max-w-prose">
          {data.description}
        </p>
      </Hero>
      <section class="flex flex-col z-20">
        <Alternatives data={data.alternatives} />
      </section>
      <script src="/orb.js" type="module" />
    </>
  );
}
