export default {
  name: "requirement",
  type: "object",
  title: "Requirement",
  fields: [
    {
      name: "name",
      type: "localeString",
      title: "Name",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "placeholder",
      type: "localeString",
      title: "Placeholder",
    },
    {
      name: "hint",
      type: "localeString",
      title: "Hint",
    },
    {
      name: "multiple",
      type: "boolean",
      title: "Multiple",
    },
    {
      name: "min",
      type: "number",
      title: "Min",
    },
    {
      name: "max",
      type: "number",
      title: "Max",
    },
    {
      name: "type",
      type: "string",
      title: "Type",
      options: {
        list: [
          { title: "Text", value: "text" },
          { title: "Number", value: "number" },
          { title: "Email", value: "email" },
          { title: "Telephone", value: "tel" },
          { title: "URL", value: "url" },
          { title: "Date", value: "date" },
          { title: "Time", value: "time" },
          { title: "File", value: "file" },
          { title: "Select", value: "select" },
          { title: "Password", value: "password" },
          { title: "Checkbox", value: "checkbox" },
          { title: "Radio", value: "radio" },
          { title: "Currency", value: "currency" },
          { title: "Range", value: "range" },
          { title: "Color", value: "color" },
        ],
      },
      validation: (Rule) => Rule.required(),
    },
    {
      name: "validation",
      type: "string",
      title: "Validation",
    },
    {
      name: "optional",
      type: "boolean",
      title: "Optional",
    },
    {
      name: "value",
      type: "string",
      title: "Default Value",
    },
    {
      name: "options",
      type: "array",
      title: "Options",
      of: [{ type: "option" }],
    },
  ],
};
