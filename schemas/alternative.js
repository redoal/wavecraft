import { baseLanguage } from "./languages.js";
export default {
  name: "alternative",
  type: "object",
  title: "Alternative",
  fields: [
    {
      name: "name",
      type: "localeString",
      title: "Name",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "description",
      type: "localeText",
      title: "Description",
    },
    {
      name: "enabled",
      type: "boolean",
      title: "Enabled",
      initialValue: true,
    },
    {
      name: "action",
      type: "url",
      title: "Action",
      validation: (Rule) => Rule.uri({ relativeOnly: true }).required(),
      initialValue: "/",
    },
    {
      name: "domain",
      type: "string",
      title: "Domain",
      options: {
        list: [
          { title: "Internal", value: "internal" },
          { title: "Public", value: "public" },
          { title: "Private", value: "private" },
        ],
      },
      initialValue: "public",
    },
    {
      name: "support",
      type: "array",
      title: "Support",
      of: [{ type: "reference", to: [{ type: "organization" }] }],
    },
    {
      name: "consequence",
      type: "array",
      title: "Consequence",
      of: [{ type: "localeString" }],
    },
    {
      name: "encouragement",
      type: "localeString",
      title: "Encouragement",
    },
    {
      name: "features",
      type: "array",
      title: "Features",
      of: [{ type: "feature" }],
    },
  ],
  preview: {
    select: {
      title: `name.${baseLanguage.id}`,
      subtitle: `description.${baseLanguage.id}`,
    },
  },
};
