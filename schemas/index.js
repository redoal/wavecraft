import question from "./question.js";
import alternative from "./alternative.js";
import criteria from "./criteria.js";
import feature from "./feature.js";
import organization from "./organization.js";
import requirement from "./requirement.js";
import option from "./option.js";
import { localeString, localeText } from "./languages.js";

export const schemaTypes = [
  question,
  alternative,
  criteria,
  feature,
  organization,
  requirement,
  option,
  localeString,
  localeText,
];
