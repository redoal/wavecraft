export default {
  name: "organization",
  type: "document",
  title: "Organization",
  fields: [
    {
      name: "name",
      type: "string",
      title: "Name",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "url",
      type: "url",
      title: "URL",
    },
    {
      name: "logo",
      type: "image",
      title: "Logo",
    },
    {
      name: "relation",
      type: "text",
      title: "Relation",
    },
    {
      name: "domain",
      type: "string",
      title: "Domain",
      options: {
        list: [
          { title: "Internal", value: "internal" },
          { title: "Public", value: "public" },
          { title: "Private", value: "private" },
        ],
      },
    },
  ],
};
