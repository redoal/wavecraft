export default {
  name: "option",
  type: "object",
  title: "Option",
  fields: [
    {
      name: "key",
      type: "string",
      title: "Key",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "Value",
      type: "localeString",
      title: "Value",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "label",
      type: "localeString",
      title: "Label",
      validation: (Rule) => Rule.required(),
    },
  ],
};
