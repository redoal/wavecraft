export const supportedLanguages = [
  { id: "en", title: "English", isDefault: true },
  { id: "no", title: "Norwegian" },
  { id: "se", title: "Swedish" },
  { id: "dk", title: "Danish" },
];

export const baseLanguage = supportedLanguages.find((l) => l.isDefault);

export const localeText = {
  title: "Localized text",
  name: "localeText",
  type: "object",
  fieldsets: [
    {
      title: "Translations",
      name: "translations",
      options: { collapsible: true, collapsed: true },
    },
  ],
  fields: supportedLanguages.map((lang) => ({
    title: lang.title,
    name: lang.id,
    type: "text",
    fieldset: lang.isDefault ? null : "translations",
  })),
};

export const localeString = {
  title: "Localized string",
  name: "localeString",
  type: "object",
  fieldsets: [
    {
      title: "Translations",
      name: "translations",
      options: { collapsible: true, collapsed: true },
    },
  ],
  fields: supportedLanguages.map((lang) => ({
    title: lang.title,
    name: lang.id,
    type: "string",
    fieldset: lang.isDefault ? null : "translations",
  })),
};
