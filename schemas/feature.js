export default {
  name: "feature",
  type: "object",
  title: "Feature",
  fields: [
    {
      name: "name",
      type: "localeString",
      title: "Name",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "description",
      type: "localeText",
      title: "Description",
    },
    {
      name: "requirements",
      type: "array",
      title: "Requirements",
      of: [{ type: "requirement" }],
    },
    {
      name: "Resource",
      type: "array",
      title: "Resource",
      of: [],
    },
  ],
};
