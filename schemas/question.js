import { baseLanguage } from "./languages";
export default {
  name: "question",
  type: "document",
  title: "Question",
  fields: [
    {
      name: "question",
      type: "localeString",
      title: "Question",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "approved",
      type: "boolean",
      title: "Approved",
    },
    {
      name: "description",
      type: "localeText",
      title: "Description",
    },
    {
      name: "alternatives",
      type: "array",
      title: "Alternatives",
      of: [{ type: "alternative" }],
      validation: (Rule) => Rule.required(),
    },
    {
      name: "criterias",
      type: "array",
      title: "Criterias",
      of: [{ type: "criteria" }],
    },
  ],
  preview: {
    select: {
      title: `question.${baseLanguage.id}`,
      subtitle: "approved",
    },
  },
};
