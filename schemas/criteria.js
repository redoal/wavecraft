export default {
  name: "criteria",
  type: "object",
  title: "Criteria",
  fields: [
    {
      name: "name",
      type: "localeString",
      title: "Name",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "description",
      type: "localeText",
      title: "Description",
    },
    {
      name: "answered",
      type: "array",
      title: "Answered",
      of: [{ type: "question" }, { type: "alternative" }],
    },
  ],
};
