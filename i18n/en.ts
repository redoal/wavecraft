import { initTranslation, load } from "https://deno.land/x/t_i18n/mod.ts";

const enTranslation = {
  welcome: "Welcome",
  myName: "My name is {first} {last}",
  myNameNestedArgs: "My name is {user.name.first} {user.name.last}",
  this: {
    is: {
      nested: "this is nested",
      nestedWith: { args: "this should show nested arg named {arg.name}" },
    },
  },
  unsafe: "this is unsafe {html}",
  unsafeButDoNotEscape: "this is unsafe {{html}}",
};

load("en", enTranslation);
