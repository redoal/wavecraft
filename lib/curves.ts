type Point = number[];
type Polychain = Point[];

function calculateTurningFunction(polyChain: Polychain): Polychain {
  const turningFunction: Polychain = [];

  for (let i = 1; i < polyChain.length - 1; i++) {
    const angle = Math.atan2(
      polyChain[i + 1][1] - polyChain[i][1],
      polyChain[i + 1][0] - polyChain[i][0],
    ) -
      Math.atan2(
        polyChain[i][1] - polyChain[i - 1][1],
        polyChain[i][0] - polyChain[i - 1][0],
      );
    const distance = Math.sqrt(
      Math.pow(polyChain[i + 1][0] - polyChain[i][0], 2) +
        Math.pow(polyChain[i + 1][1] - polyChain[i][1], 2),
    );
    turningFunction.push([angle, distance]);
  }
  return turningFunction;
}

export default function LSH(polyChain: Polychain, bands: number) {
  const turningFunction = calculateTurningFunction(polyChain);
  const signatures = minHash(turningFunction.flat(), 30);
  const clusters = kMeansClustering(createBands(signatures, bands));
  return { turningFunction, signatures, clusters };
}

function createBands(matrix, r) {
  const bandMatrix = [];
  for (let i = 0; i < matrix.length; i += r) {
    bandMatrix.push(matrix.slice(i, i + r));
  }
  return bandMatrix;
}

function minHash(values: number[], hashes: number) {
  const hashFns = Array.from(
    { length: hashes },
    (_, i) => (p) => Math.abs(((i + 1) * p + 3 * i) % 101),
  );

  return hashFns.map((hashFn) => Math.min(...values.map(hashFn)));
}

function kMeansClustering(data, k = 2) {
  let centroids = data.slice(0, k);

  const distance = (a, b) => {
    let sum = 0;
    for (let i = 0; i < a.length; i++) {
      sum += Math.pow(a[i] - b[i], 2);
    }
    return Math.sqrt(sum);
  };

  const assignToCentroids = () => {
    const groups = [];
    for (let i = 0; i < k; i++) groups[i] = [];

    for (const point of data) {
      let bestIndex = 0;
      let minDistance = Infinity;
      for (let i = 0; i < centroids.length; i++) {
        const dist = distance(point, centroids[i]);
        if (dist < minDistance) {
          minDistance = dist;
          bestIndex = i;
        }
      }
      groups[bestIndex].push(point);
    }
    return groups;
  };

  const computeCentroids = (groups) => {
    return groups.map((group) => {
      const arr = group[0].map((x, i) => group.map((x) => x[i]));
      return arr.map((x) => x.reduce((a, b) => a + b) / x.length);
    });
  };

  let oldCentroids = [];
  let groups = [];

  while (!arraysEqual(oldCentroids, centroids)) {
    oldCentroids = centroids;
    groups = assignToCentroids();
    centroids = computeCentroids(groups);
  }
  return groups;
}

function arraysEqual(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length != b.length) return false;

  for (let i = 0; i < a.length; ++i) {
    if (Array.isArray(a[i]) && Array.isArray(b[i])) {
      if (!arraysEqual(a[i], b[i])) return false;
    } else if (a[i] !== b[i]) return false;
  }
  return true;
}
