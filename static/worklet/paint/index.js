if ("paintWorklet" in CSS) {
  loadWorklets();
} else {
  const script = document.createElement("script");
  script.addEventListener("load", loadWorklets);
  script.setAttribute("src", "https://unpkg.com/css-paint-polyfill");
  document.head.appendChild(script);
}

function loadWorklets() {
  const pathPrefix = "/worklet/paint/";
  CSS.paintWorklet.addModule(pathPrefix + "starry.js");
  CSS.paintWorklet.addModule(pathPrefix + "paths.js");
}
