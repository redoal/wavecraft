
class Paths {
  static get inputProperties() {
    return ["--path"];
  }
  paint(ctx, size, properties) {
    if (!this.paths?.length) this.paths = []
    const pathCount = Math.round(size.width * size.height / 10000)
    ctx.clearRect(0, 0, size.width, size.height);
    while (this.paths.length < pathCount) {
      this.paths.push(turningFunctionToPath(createTurningFunction(8), size));
    }
    while (this.paths.length > pathCount) this.paths.pop();
    ctx.strokeStyle = "rgba(0, 0, 0, 0.6)";
    ctx.lineWidth = 0.4;
    ctx.translate(size.width / 2, size.height / 2);
    for (let i = 0; i < this.paths.length; ++i) {
      ctx.beginPath();
      ctx.stroke(this.paths[i]);
    }
  }
}

registerPaint("paths", Paths);

function turningFunctionToPath(turning, size) {
  const path = new Path2D();
  let currentRadian = turning[0]
  let x = Math.cos(turning[0]) * turning[1];
  let y = Math.sin(turning[0]) * turning[1];
  console.log('starting point path', x, y, size)
  path.moveTo(x * size.width, y * size.height);
  // loop over raimaining points and smooth path using angle difference and distance of the previous points in the turning function
  for (let i = 2; i < turning.length; i += 2) {
    currentRadian += turning[i];
    const xDiff = Math.cos(currentRadian) * turning[i + 1]
    const yDiff = Math.sin(currentRadian) * turning[i + 1]
    // create arch if if we have enough points
    if (i > 4) {
      let lastRadian = currentRadian - turning[i];
      if (lastRadian < 0) {
        lastRadian = lastRadian % Math.PI * 2
      }
      path.arcTo(x * size.width, y * size.height, x + xDiff * size.width, yDiff * size.height, Math.abs(lastRadian))
    } else {
      path.lineTo(x + xDiff * size.width, y + yDiff * size.height);
    }
    x += xDiff;
    y += yDiff;
  }
  return path
}

function createTurningFunction(stopCount) {
  // turning function is a flat array of floats consisting of angledifference and distaince traveled where 1 is the width or height of the canvas
  if (stopCount < 2) return [];
  const turning = [];
  // set up x and y tracking values
  let x = 0;
  let y = 0;
  // use vector floats for distance and radian angle 
  // indicate starting position from center of canvas
  turning.push(Math.random() * Math.PI * 2, Math.random() * 0.5)
  // add on the remainder of the stops making sure the x and y are values remain between 0 and 1 
  let radianSum = turning[0];
  x += Math.cos(turning[0]) * turning[1];
  y += Math.sin(turning[0]) * turning[1];
  for (let i = 2; i < stopCount + 2; ++i) {
    let shiftAngle, shiftDistance, newRadianSum;
    // try a few times to get it to stay within the confines of the frame
    for (let j = 0; i < 1 || !isInBounds(x + (Math.cos(newRadianSum) * shiftDistance), y + (Math.sin(newRadianSum) * shiftDistance)); j++) {
      shiftAngle = Math.pow(Math.random(), Math.random()) * Math.PI * 2 * (Math.random() > 0.5 ? 1 : -1)
      shiftDistance = Math.random() * 0.5
      newRadianSum = (radianSum + shiftAngle) % Math.PI * 2
      if (j > 300) { 
        console.log('stepped out', x, y)
        break
      }
    }
    radianSum = newRadianSum
    x += Math.cos(radianSum) * shiftDistance
    y += Math.sin(radianSum) * shiftDistance
    turning.push(shiftAngle, shiftDistance)
  }
  return turning; 
}

function isInBounds(x, y) {
  return x > 0 && x < 1 && y > 0 && y < 1
}
