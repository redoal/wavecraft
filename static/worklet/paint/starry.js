class Starry {
  static get inputProperties() {
    return ["--path"];
  }
  paint(ctx, size, properties) {
    console.log(ctx, size, properties);
    if (!this.stars) this.stars = [];
    let starCount = size.width * size.height / 1000;
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, size.width, size.height);
    ctx.fillStyle = "white";
    while (this.stars.length < starCount) {
      this.stars.push(Math.random(), Math.random());
    }
    while (this.stars.length > starCount) this.stars.pop();
    for (let i = 0; i < this.stars.length; i += 2) {
      ctx.beginPath();
      ctx.arc(
        this.stars[i] * size.width,
        this.stars[i + 1] * size.height,
        1,
        0,
        2 * Math.PI,
      );
      ctx.fill();
    }

    const path = properties.get("--path").toString().split(",").map((x) =>
      parseFloat(x)
    );

    if (path.length < 4) return;
    ctx.strokeStyle = "white";
    // define scale factor to fit path within canvas
    const scaleFactor = Math.min(
      size.width / (Math.max(...path) - Math.min(...path)),
      size.height / (Math.max(...path.slice(1)) - Math.min(...path.slice(1))),
    ) - .3;
    // find center of path
    const center = path.reduce((acc, cur, i) => {
      if (i % 2 === 0) acc[0] += cur;
      else acc[1] += cur;
      return acc;
    }, [0, 0]).map((x) => x / (path.length / 2));

    // draw in center of canvas and scale to fit within size height and width
    ctx.translate(
      size.width / 2 - center[0] * scaleFactor,
      size.height / 2 - center[1] * scaleFactor,
    );
    ctx.beginPath();
    ctx.moveTo(path[0] * scaleFactor, path[1] * scaleFactor);
    for (let i = 2; i < path.length; i += 2) {
      ctx.lineTo(path[i] * scaleFactor, path[i + 1] * scaleFactor);
    }
    ctx.stroke();
  }
}

registerPaint("starry", Starry);
