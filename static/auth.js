async function init() {
  // Set a randomUUID for the user if they don't have one
  if (!localStorage.getItem("uuid")) {
    localStorage.setItem("uuid", Crypto.randomUUID());
  }

  // Get the user's uuid
  const uuid = localStorage.getItem("uuid");

  // Set session storage for the user
  sessionStorage.setItem("uuid", uuid);

  // If the user has a keypair in localStorage, use it
  if (localStorage.getItem("keyPair")) {
    const base64keyPair = JSON.parse(localStorage.getItem("keyPair"));
    const keyPair = await initExisting(base64keyPair);
  } else {
    // use webcrypto to generate a key pair
    const keyPair = await window.crypto.subtle.generateKey(
      {
        name: "RSA-OAEP",
        modulusLength: 2048,
        publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
        hash: { name: "SHA-256" },
      },
      true,
      ["encrypt", "decrypt"],
    );
    // export the public key
    const publicKey = await window.crypto.subtle.exportKey(
      "spki",
      keyPair.publicKey,
    );
    // export the private key
    const privateKey = await window.crypto.subtle.exportKey(
      "pkcs8",
      keyPair.privateKey,
    );

    // base64 encode the public and private keys
    const base64keyPair = {
      publicKey: String.fromCharCode(...new Uint8Array(publicKey)),
      privateKey: String.fromCharCode(...new Uint8Array(privateKey)),
    };

    // store the keypair in localStorage
    localStorage.setItem(
      "keyPair",
      JSON.stringify(base64keyPair),
    );
    // send the user's uuid and publicKey to server
    // and store the server's public key in localStorage
    const serverPublicKey = await fetch("/api/auth", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        uuid,
        publicKey: base64keyPair.publicKey,
      }),
    }).then((res) => res.json());
  }

  // localStorage.setItem("serverPublicKey", JSON.stringify(serverPublicKey));
}

// init();

async function initExisting(keyPair) {
  const publicKey = await window.crypto.subtle.importKey(
    "spki",
    Uint8Array.from(atob(keyPair.publicKey), (c) => c.charCodeAt(0)),
    {
      name: "RSA-OAEP",
      hash: { name: "SHA-256" },
    },
    true,
    ["encrypt"],
  );
  const privateKey = await window.crypto.subtle.importKey(
    "pkcs8",
    Uint8Array.from(atob(keyPair.privateKey), (c) => c.charCodeAt(0)),
    {
      name: "RSA-OAEP",
      hash: { name: "SHA-256" },
    },
    true,
    ["decrypt"],
  );
  return { publicKey, privateKey };
}
