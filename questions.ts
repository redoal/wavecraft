export type Question = {
  name: string;
  description: string;
  criterias?: Criteria[];
  approvedForProduction?: boolean;
  alternatives: Alternative[];
};

export type Alternative = {
  name: string;
  description: string;
  disabled?: boolean;
  action?: string;
  domain?: Domain;
  support?: Organization[];
  consequence?: string[];
  encouragement?: string;
  features: Feature[];
};

export type Feature = {
  name: string;
  description: string;
  requirements?: Requirement[];
  resource?: Resource;
};

export type Requirement = {
  name: string;
  placeholder: string;
  hint?: string;
  multiple?: boolean;
  min?: number | string;
  max?: number | string;
  type:
    | "text"
    | "number"
    | "email"
    | "tel"
    | "url"
    | "date"
    | "time"
    | "file"
    | "select"
    | "password"
    | "checkbox"
    | "radio"
    | "currency"
    | "range"
    | "curve"
    | "color";
  validation?: string;
  optional?: boolean;
  value?: string;
  options?: SelectOption[];
};
export enum Domain {
  Public = "public",
  Private = "private",
  Internal = "internal",
}

type Organization = {
  name: string;
  logo?: string;
  domain?: URL;
};

type Resource = {
  name: string;
  path: string;
};

type SelectOption = {
  value: string;
  label: string;
};

// a single word for ad distribution could be

export const questions: Question[] = [
  {
    name: "What shapes us?",
    description:
      "Promoting an archaic human-operated distributed language model",
    alternatives: [
      {
        name: "Our Understanding",
        action: "shape",
        disabled: true,
        description:
          "By associating similar movements while facilitating discourse we discover signal within chaos",
        domain: Domain.Public,
        consequence: ["Find what follows"],
        encouragement: "What you don't know – your collaborators will…",
        features: [
          {
            name: "",
            description: "Start by creating a path",
            requirements: [
              {
                name: "Curve",
                placeholder: "Lines",
                hint: "Just like magic",
                type: "curve",
              },
            ],
          },
        ],
      },
      {
        name: "Our Composition",
        action: "compose",
        disabled: true,
        description:
          "Bring your collaborators to the next level by contributing",
        domain: Domain.Private,
        features: [
          {
            name: "Enhance discussions",
            description:
              "Enlighten, gather feedback, improve, and ultimately find community across your usual borders",
          },
          {
            name: "Develop social features",
            description:
              "We're highly dialogue driven and think you can be too",
            requirements: [],
          },
          {
            name: "Craft templates for communities",
            description:
              "Organize your community assets and choose desired interaction mechanics",
          },
          {
            name: "Prosper from experience",
            description:
              "The best way to learn is to teach, profit from your knowledge",
          },
        ],
        consequence: ["Tune in"],
        encouragement: "No code required",
      },
      {
        name: "Our Devotion",
        action: "devotion",
        disabled: true,
        description: "Become part of a great and caring community",
        domain: Domain.Internal,
        consequence: ["Get to know us"],
        encouragement: "We might be hiring!",
        features: [
          {
            name: "Offices",
            description:
              "Our physical spaces surrounds the globe, and we're a fully distributed company – but if you really want to say hallow to the executives – they're in Oslo most of the time",
          },
          {
            name: "Teams",
            description:
              "Fueling space takes many disiplines from a wide set of people – we're friendly and service minded with a right to say no",
            requirements: [],
          },
          {
            name: "Complexity",
            description:
              "Learn from our experience in building a complex system that is both performant, approachable and scalable",
            requirements: [],
          },
          {
            name: "Culture",
            description:
              "Obsessing on signal processing and being our own customer can be a daunting task, but we hope to reach understanding one day – vroom vroom!",
            requirements: [],
          },
        ],
        support: [{
          name: "Bendik",
        }, {
          name: "Discord",
          logo:
            "https://assets-global.website-files.com/6257adef93867e50d84d30e2/653714c17467993e7b389c83_636e0a6918e57475a843f59f_icon_clyde_black_RGB.svg",
          domain: new URL("https://discord.gg/cCWqNd4v"),
        }, {
          name: "Antler",
          logo:
            "https://assets-global.website-files.com/62d19f82a58657afab15127b/634e9d1bdcd609e15463853e_antler-logo-white.svg",
          domain: new URL("https://www.antler.co"),
        }],
      },
    ],
  },
];
