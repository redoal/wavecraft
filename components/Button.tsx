import { IS_BROWSER } from "$fresh/runtime.ts";

export function Button(props: HTMLAttributes<HTMLButtonElement>) {
  return (
    <button
      {...props}
      disabled={!IS_BROWSER || props.disabled}
      class="border shadow pointer-events-auto leading-5 self-end self-end rounded focus:bg-[#00f2f2] hover:bg-[#00f2f2] p-4 outline-none border text-black bg-gray-200"
    />
  );
}
