export function Logo(props): JSX.Element {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="400"
      viewBox="0 0 400 400"
      stroke="none"
      {...props}
      height="400"
    >
      <defs>
        <circle id="c" cx="200" cy="200" r="180" fill="currentColor" />
        <clipPath id="m">
          <use href="#c" />
        </clipPath>
        <path
          id="sine-swings"
          fill="none"
          d="M 0 200 Q 101 100 200 200 Q 300 300 400 200 Q 500 100 600 200 Q 700 300 800 200"
          stroke="white"
        />
      </defs>
      <use href="#c" />
      <g
        clip-path="url(#m)"
        style="transform: scale(0.8) rotate(-13deg); transform-origin: center;"
      >
        <use href="#sine-swings" x="0" y="0" stroke-width="6.5">
          <animate
            attributeName="x"
            from="0"
            to="-400"
            dur="3s"
            repeatCount="indefinite"
          />
        </use>
        <use href="#sine-swings" x="0" y="7" stroke-width="3">
          <animate
            attributeName="x"
            from="0"
            to="-400"
            dur="3s"
            repeatCount="indefinite"
          />
        </use>
        <use href="#sine-swings" x="0" y="9" stroke-width="1">
          <animate
            attributeName="x"
            from="0"
            to="-400"
            dur="3s"
            repeatCount="indefinite"
          />
        </use>
        <use href="#sine-swings" x="0" y="15" stroke-width=".5">
          <animate
            attributeName="x"
            from="0"
            to="-400"
            dur="3s"
            calcMode="paced"
            repeatCount="indefinite"
          />
        </use>
      </g>
    </svg>
  );
}
