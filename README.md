# Redoal - `/ʁiˈtu̯aːl/`

A human operated low-dimentional small language model where we weigh movements
with dialogue.

By measuring input distance from what the network knows we intend to resolve
most common paths to a contributor of the dialogue.

### Usage

Start the dev server:

```
deno task start
```

### Questions

As part of our onboarding process we ask leading questions with helpful
answer-alternatives listing it's features and optional requirements

Find both types and landing page questions in `questions.ts`
