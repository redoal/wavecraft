import { Options } from "$fresh/plugins/twindv1.ts";
import * as colors from "twind/colors";

export default {
  selfURL: import.meta.url,
  colors: colors,
  extend: {
    theme: {
      fontFamily: {
        "sans": ["LeagueMono", "sans-serif"],
        "serif": ["LeagueMono", "sans-serif"],
      },
    },
  },
} as Options;
